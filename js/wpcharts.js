// jQuery to adjust element heights to equal each others
	jQuery(document).ready(function($) { //noconflict wrapper
	    var heights = $(".same-height").map(function() {
	        return $(this).height();
	    }).get(),
	    maxHeight = Math.max.apply(null, heights);
	    $(".same-height").height(maxHeight);
	});
// jQuery for KPI Progress Bars
	$(function () { 
	  $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
	});   
	  $(".progress-bar").each(function(){
	    each_bar_width = $(this).attr('aria-valuenow');
	    $(this).width(each_bar_width + '%');
	  });
// CHARTS 
// To be used with Chart.js - Currently Static, need API to populate datasets
// Revenue vs Expenses
	var REchart = document.getElementById("RevenuesVsExpenses").getContext("2d");
	var RevExp = new Chart(REchart, {
		type: 'bar',
		data: {
	        labels: ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
	        datasets: [
		        {
		        	type: 'bar',
		            label: 'Revenue',
		            data: [4000,3000,5500,4500,8000,12000],
		            backgroundColor: '#23aacb',
		        },
		        {
		        	type: 'bar',
		            label: 'Expense',
		            data: [10000,11000,9000,10000,11000,10000],
		            backgroundColor: '#d19333',
		        },
		        {
		        	type: 'bar',
		            label: 'Profit',
		            data: [-6000,-8000,-3500,-4500,-3000,2000],
		            backgroundColor: '#806eac',
		        }
		    ]
		},
		options: {
	        responsive: false,
	        legend: {
	        	display: false,
	        },
	        tooltips: {
	            mode: 'index',    
	        },
	        scales: {
	            yAxes: [{
	                id: "y-axis-1",
	                position: "left",
	                gridLines: {
	                	color: "#576075",
	                	borderDash: [5,5],
	                	zeroLineWidth: 1,
	                	zeroLineColor: '#b5bbc8'
	                },
	                ticks: {
	                	stepSize: 5000
	                }
	            }],
	            xAxes: [{
	            	barPercentage: 1,
	            	categoryPercentage: 0.5,
	            }]
	        }
	    }
	});
	document.getElementById('RevExp-Lgd').innerHTML = RevExp.generateLegend();

// Revenue vs COGS
	var RCchart = document.getElementById("RevenuesVsCOGS").getContext("2d");
	var RevCOGS = new Chart(RCchart, {
		type: 'bar',
		data: {
	        labels: ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
	        datasets: [
		        {
		        	type: 'bar',
		            label: 'Revenue',
		            data: [4000,3000,5500,4500,8000,12000],
		            backgroundColor: '#23aacb',
		        },
		        {
		        	type: 'bar',
		            label: 'COGS',
		            data: [5000,3000,5000,4000,6000,9000],
		            backgroundColor: '#d14764',
		        },
		        {
		        	type: 'bar',
		            label: 'G. Profit',
		            data: [-1000,0,500,500,2000,3000],
		            backgroundColor: '#32D123',
		        }
		    ]
		},
		options: {
	        responsive: true,
	        legend: {
	        	display: false,
	        },
	        tooltips: {
	            mode: 'index',    
	        },
	        scales: {
	            yAxes: [{
	                id: "y-axis-1",
	                position: "left",
	                gridLines: {
	                	color: "#576075",
	                	borderDash: [5,5],
	                	zeroLineWidth: 1,
	                	zeroLineColor: '#b5bbc8'
	                },
	                ticks: {
	                	stepSize: 5000
	                }
	            }],
	            xAxes: [{
	            	barPercentage: 1,
	            	categoryPercentage: 0.5,
	            }]
	        }
	    }
	});
	document.getElementById('RevCOGS-Lgd').innerHTML = RevCOGS.generateLegend();

// Sparkline Revenue
	var GMspark = document.getElementById("GrossMarginSpark").getContext("2d");
	var GrossMarginSpark = new Chart(GMspark, {
		type: 'line',
		data: {
	        labels: ["Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
	        datasets: [
		        {
		        	type: 'line',
		        	lineTension: 0,
		        	pointRadius: 0,
		        	fill: false,
		            label: 'Revenue',
		            data: [-0.15,0,0.10,0.10,0.30,0.4],
		            borderColor: '#23aacb',
		            borderWidth: 2,
		        }
		    ]
		},
		options: {
	        responsive: true,
	        legend: { display: false },
	        tooltips: { enabled: false },

	        scales: {
	            yAxes: [{ display: false }],
	            xAxes: [{ display: false }]
	        }
	    }
	});

	// sortable functions
	$( function() {
		$( "#sortable-panel" ).sortable({ tolerance: 'pointer' });
	});

	$( function() {
		$( "#sortable-panel-body-rev-gr" ).sortable({ tolerance: 'pointer' });
	});
	$( function() {
		$( "#sortable-panel-body-rev-gr-col" ).sortable({ tolerance: 'pointer' });
	});

	$( function() {
		$( "#sortable-panel-body-inc-gr" ).sortable({ tolerance: 'pointer' });
	});
	$( function() {
		$( "#sortable-panel-body-inc-gr-col" ).sortable({ tolerance: 'pointer' });
	});

	$( function() {
		$( "#sortable-panel-body-cos-gr-col" ).sortable({ tolerance: 'pointer' });
	});
	
	
	